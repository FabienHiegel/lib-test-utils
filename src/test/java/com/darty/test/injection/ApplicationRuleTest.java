package com.darty.test.injection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.inject.Inject;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.util.MockUtil;

import com.darty.test.server.MockMainModule;
import com.darty.test.server.MockConfiguration;

public class ApplicationRuleTest {

    @ClassRule
    public static ApplicationRule<MockConfiguration> application = ApplicationRule
            .prepareApplication(MockConfiguration.class, "src/test/resources/configs/mock-configuration.yaml")
            .addDependancy(MockMainModule::new)
            .build();

    @Rule
    public MethodRule injection = application.inject();

    static class MyMockedObject {
        void doSomeStuff(String input) {
        }
    }

    static class MySpyiedObject {
        void doSomeOtherStuff(String input) {
        }
    }

    static class MyInjectedObject {

    }

    static class AnObject {
        @Inject
        MyMockedObject injectedMock;
        @Inject
        MySpyiedObject injectedSpy;
        @Inject
        MyInjectedObject injectedObject;
    }

    @Mock
    private MyMockedObject mocked;

    @Spy
    private MySpyiedObject spyied;

    @Inject
    private MyInjectedObject injectedObject;

    @Inject
    private AnObject anObject;

    @Captor
    private ArgumentCaptor<String> aCaptor;

    @Test
    public void should_mocked_annotated_object_by_mocked() throws Exception {
        assertThat(MockUtil.isMock(mocked)).isTrue();
    }

    @Test
    public void should_spyied_annotated_object_by_spied() throws Exception {
        assertThat(MockUtil.isSpy(spyied)).isTrue();
    }

    @Test
    public void should_injected_annotated_object_by_injected() throws Exception {
        assertThat(injectedObject).isNotNull();
    }

    @Test
    public void should_object_injected_field_be_injected() throws Exception {
        assertThat(anObject.injectedObject).isNotNull();
    }

    @Test
    public void should_object_injected_and_mocked_field_be_a_mock() throws Exception {
        assertThat(MockUtil.isMock(anObject.injectedMock)).isTrue();
    }

    @Test
    public void should_object_injected_and_spyied_field_be_a_spy() throws Exception {
        assertThat(MockUtil.isSpy(anObject.injectedSpy)).isTrue();
    }

    @Test
    public void should_verify_mock() throws Exception {
        mocked.doSomeStuff("");
        verify(mocked, times(1)).doSomeStuff("");
    }

    @Test
    public void should_verify_mock_once_when_two_methods_uses_same_injected_mock() throws Exception {
        mocked.doSomeStuff("");
        verify(mocked, times(1)).doSomeStuff("");
    }

    @Test
    public void should_verify_spy() throws Exception {
        spyied.doSomeOtherStuff("");
        verify(spyied, times(1)).doSomeOtherStuff("");
    }

    @Test
    public void should_verify_spy_once_when_two_methods_uses_same_injected_mock() throws Exception {
        spyied.doSomeOtherStuff("");
        verify(spyied, times(1)).doSomeOtherStuff("");
    }

    @Test
    public void should_capture() throws Exception {
        mocked.doSomeStuff("some input");

        verify(mocked, times(1)).doSomeStuff(aCaptor.capture());
        assertThat(aCaptor.getAllValues()).containsExactly("some input");
    }

    @Test
    public void should_capture_only_what_happend_in_test_when_capture_in_two_methods() throws Exception {
        mocked.doSomeStuff("some other input");

        verify(mocked, times(1)).doSomeStuff(aCaptor.capture());
        assertThat(aCaptor.getAllValues()).containsExactly("some other input");
    }

}
