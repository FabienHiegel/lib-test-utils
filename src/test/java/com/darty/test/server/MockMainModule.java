package com.darty.test.server;

import com.darty.utils.dropwizard.DartyGuiceModule;

import io.dropwizard.setup.Environment;

public class MockMainModule extends DartyGuiceModule<MockConfiguration> {

    public MockMainModule(MockConfiguration configuration, Environment environment) {
        super(environment, configuration);
    }

    @Override
    protected void addDependancies(MockConfiguration conf, Environment environment) {
    }

    @Override
    protected void configure(MockConfiguration conf, Environment env) {
    }

}