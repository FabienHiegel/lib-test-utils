package com.darty.test.configuration;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

public class ConfigurationDescription<C extends Configuration> {
    final Class<C> configurationClass;
    final String configurationPath;

    public ConfigurationDescription(Class<C> configurationClass, String configurationPath) {
        this.configurationClass = configurationClass;
        this.configurationPath = configurationPath;
    }

    public C createConfiguration(Environment environment) {
        return ConfigurationUtils.createConfiguration(environment, this);
    }
}