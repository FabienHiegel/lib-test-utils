package com.darty.test.configuration;

import java.io.File;

import io.dropwizard.Configuration;
import io.dropwizard.configuration.ConfigurationFactory;
import io.dropwizard.setup.Environment;

class ConfigurationUtils {
    private static final String CONFIGURATION_PREFIX = "";

    public static <C extends Configuration> C createConfiguration(Environment environment, ConfigurationDescription<C> configurationDescription) {
        return createConfiguration(environment, configurationDescription.configurationClass, configurationDescription.configurationPath);
    }

    public static <C extends Configuration> C createConfiguration(Environment environment, Class<C> configurationClass,
            String configurationPath) {
        try {
            return getConfigurationFactory(environment, configurationClass).build(getConfigurationFile(configurationPath));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static <C extends Configuration> ConfigurationFactory<C> getConfigurationFactory(Environment environment,
            Class<C> configurationClass) {
        return new ConfigurationFactory<>(configurationClass, environment.getValidator(), environment.getObjectMapper(),
                CONFIGURATION_PREFIX);
    }

    private static File getConfigurationFile(String configurationPath) {
        return new File(configurationPath);
    }
}
