package com.darty.test.injection;

import com.google.inject.Module;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

@FunctionalInterface
public interface Dependancy<C extends Configuration> {
    Module resolve(C configuration, Environment environment);
}