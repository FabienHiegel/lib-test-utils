package com.darty.test.injection;

import org.junit.runners.model.Statement;

class InjectorHandlerStatement extends Statement {

    private final Statement statement;
    private final InjectionRuleSupport handler;

    public InjectorHandlerStatement(Statement statement, InjectionRuleSupport handler) {
        this.statement = statement;
        this.handler = handler;
    }

    @Override
    public void evaluate() throws Throwable {
        handler.initialize();
        statement.evaluate();
        handler.release();
    }

}