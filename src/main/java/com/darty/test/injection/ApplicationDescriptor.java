package com.darty.test.injection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.darty.test.configuration.ConfigurationDescription;
import com.google.inject.Module;
import com.google.inject.util.Modules;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

class ApplicationDescriptor<C extends Configuration> {

    private final ConfigurationDescription<C> configurationDescription;
    private final Module module;
    private final Collection<DartyGuiceModuleDescriptor<C>> dartyModules;
    private final Collection<Dependancy<C>> dependancies;
    private final Collection<Module> overloadingModules;

    ApplicationDescriptor(ConfigurationDescription<C> configurationDescription, Module module,
            Collection<DartyGuiceModuleDescriptor<C>> dartyModules, Collection<Dependancy<C>> dependancies,
            Collection<Module> overrideModules) {
        this.configurationDescription = configurationDescription;
        this.module = module;
        this.dartyModules = dartyModules;
        this.dependancies = dependancies;
        this.overloadingModules = overrideModules;
    }

    public ApplicationDescriptor<C> add(Module module) {
        return new ApplicationDescriptor<>(this.configurationDescription, Modules.combine(this.module, module), this.dartyModules,
                this.dependancies, this.overloadingModules);
    }

    public ApplicationDescriptor<C> add(Module... modules) {
        return add(Modules.combine(modules));
    }

    public ApplicationDescriptor<C> override(Module... modules) {
        Collection<Module> overrideModules = new ArrayList<>(this.overloadingModules);
        Collections.addAll(overrideModules, modules);
        return new ApplicationDescriptor<>(this.configurationDescription, this.module, this.dartyModules, this.dependancies,
                Collections.unmodifiableCollection(overrideModules));
    }

    public List<Module> resolveModules(Environment environment) {
        Module mainModule = this.module;
        if (configurationDescription != null) {
            C configuration = configurationDescription.createConfiguration(environment);
            Module dependancies = Modules.combine(resolveDependancies(configuration, environment));
            Module dartyModules = Modules.combine(resolveDartyModules(configuration, environment));
            mainModule = Modules.combine(this.module, dartyModules, dependancies);
        }
        Module completeModule = Modules.override(mainModule).with(overloadingModules);
        return Collections.singletonList(completeModule);
    }

    private List<Module> resolveDependancies(C configuration, Environment environment) {
        return dependancies.stream().map(dependancy -> dependancy.resolve(configuration, environment)).collect(Collectors.toList());
    }

    private List<Module> resolveDartyModules(C configuration, Environment environment) {
        return dartyModules.stream().map(dartyModule -> dartyModule.createModule(environment, configuration)).collect(Collectors.toList());
    }

}
