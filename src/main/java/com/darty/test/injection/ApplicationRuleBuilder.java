
package com.darty.test.injection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

import com.darty.test.configuration.ConfigurationDescription;
import com.google.inject.Module;
import com.google.inject.util.Modules;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

public class ApplicationRuleBuilder<C extends Configuration> {

    private final ConfigurationDescription<C> configurationDescription;

    private Module module = Modules.EMPTY_MODULE;
    private Collection<DartyGuiceModuleDescriptor<C>> dartyModules = new ArrayList<>();
    private Collection<Dependancy<C>> dependancies = new ArrayList<>();
    private Collection<Module> overloadingModules = new ArrayList<>();

    public ApplicationRuleBuilder(ConfigurationDescription<C> configurationDescription) {
        this.configurationDescription = configurationDescription;
    }

    ApplicationRuleBuilder(Class<C> configurationClass, String configurationPath) {
        this(new ConfigurationDescription<>(configurationClass, configurationPath));
    }

    //
    // Optional
    //

    public ApplicationRuleBuilder<C> add(Module module) {
        this.module = Modules.combine(this.module, module);
        return this;
    }

    public ApplicationRuleBuilder<C> add(Module... modules) {
        return add(Modules.combine(modules));
    }

    public ApplicationRuleBuilder<C> override(Module... modules) {
        Collections.addAll(this.overloadingModules, modules);
        return this;
    }

    public ApplicationRuleBuilder<C> addDartyGuiceModule(DartyGuiceModuleDescriptor<C> moduleDescriptor) {
        dartyModules.add(moduleDescriptor);
        return this;
    }

    public ApplicationRuleBuilder<C> addDependancy(Function<Environment, Module> dependancy) {
        return addDependancy((configuration, environment) -> dependancy.apply(environment));
    }

    public ApplicationRuleBuilder<C> addDependancy(Dependancy<C> dependancy) {
        dependancies.add(dependancy);
        return this;
    }

    public ApplicationRule<C> build() {
        ApplicationDescriptor<C> applicationDescription = createApplicationDescriptor();
        return new ApplicationRule<>(applicationDescription);
    }

    private ApplicationDescriptor<C> createApplicationDescriptor() {
        return new ApplicationDescriptor<C>(configurationDescription, module, dartyModules, dependancies, overloadingModules);
    }

}
