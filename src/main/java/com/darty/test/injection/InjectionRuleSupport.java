package com.darty.test.injection;

import java.util.function.Supplier;

import com.google.inject.Injector;
import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.InjectorFactory;
import com.hubspot.dropwizard.guice.InjectorFactoryImpl;

import io.dropwizard.setup.Environment;

class InjectionRuleSupport {
    private static final InjectorFactory injectorFactory = new InjectorFactoryImpl();

    private Injector injector;
    private boolean injected;
    private Supplier<Injector> injectorProvider;

    public InjectionRuleSupport(ApplicationDescriptor<?> application, Environment environment) {
        this(() -> createInjector(application, environment));
    }

    private static Injector createInjector(ApplicationDescriptor<?> application, Environment environment) {
        return injectorFactory.create(Stage.DEVELOPMENT, application.resolveModules(environment));
    }

    public InjectionRuleSupport(Supplier<Injector> injectorProvider) {
        this.injectorProvider = injectorProvider;
    }

    public InjectionRule rule() {
        injected = true;
        return new InjectionRule(injector);
    }

    public void initialize() {
        injector = injectorProvider.get();
    }

    public void release() {
        if (!injected) {
            throw new RuntimeException("Not injected.");
        }
    }

}