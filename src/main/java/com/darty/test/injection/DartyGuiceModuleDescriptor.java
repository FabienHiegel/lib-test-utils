package com.darty.test.injection;

import com.darty.utils.dropwizard.DartyGuiceModule;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

@FunctionalInterface
public interface DartyGuiceModuleDescriptor<C extends Configuration> {
    DartyGuiceModule<C> createModule(Environment environment, C configuration);
}