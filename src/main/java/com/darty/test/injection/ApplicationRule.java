package com.darty.test.injection;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import com.darty.test.server.TestEnvironment;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

public class ApplicationRule<C extends Configuration> implements TestRule {

    private final ApplicationDescriptor<C> applicationDescriptor;
    private InjectionRuleSupport ruleSupport;

    ApplicationRule(ApplicationDescriptor<C> applicationDescriptor) {
        this.applicationDescriptor = applicationDescriptor;
    }

    public static <C extends Configuration> ApplicationRuleBuilder<C> prepareApplication(Class<C> configurationClass,
            String configurationPath) {
        return new ApplicationRuleBuilder<>(configurationClass, configurationPath);
    }

    @Override
    public Statement apply(Statement base, Description description) {
        ruleSupport = createHandler(description.getTestClass());
        return new InjectorHandlerStatement(base, ruleSupport);
    }

    private InjectionRuleSupport createHandler(Class<?> testClass) {
        ApplicationDescriptor<C> application = getApplication(testClass);
        Environment environment = getEnvironment(testClass);
        return new InjectionRuleSupport(application, environment);
    }

    private ApplicationDescriptor<C> getApplication(Class<?> testClass) {
        return applicationDescriptor.override(new InjectClassMockedFieldModule(testClass));
    }

    private TestEnvironment getEnvironment(Class<?> testClass) {
        return new TestEnvironment();
    }

    public InjectionRule inject() {
        return ruleSupport.rule();
    }

}
