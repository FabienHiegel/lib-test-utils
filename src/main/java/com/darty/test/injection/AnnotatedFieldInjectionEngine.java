package com.darty.test.injection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.mockito.internal.configuration.FieldAnnotationProcessor;

class AnnotatedFieldInjectionEngine {

    private final static FieldAnnotationProcessor<?> NOOP_PROCESSOR = (annotation, field) -> null;

    private final Map<Class<? extends Annotation>, FieldAnnotationProcessor<?>> annotationProcessorMap = new HashMap<Class<? extends Annotation>, FieldAnnotationProcessor<?>>();

    //
    // Annotation processors register
    //

    public <A extends Annotation> AnnotationRegisterBuilder<A> forAnnotation(Class<A> annotationClass) {
        return new AnnotationRegisterBuilder<>(annotationClass);
    }

    public class AnnotationRegisterBuilder<A extends Annotation> {
        private Class<A> annotationClass;

        private AnnotationRegisterBuilder(Class<A> annotationClass) {
            this.annotationClass = annotationClass;
        }

        public AnnotatedFieldInjectionEngine process(FieldAnnotationProcessor<A> fieldAnnotationProcessor) {
            annotationProcessorMap.put(annotationClass, fieldAnnotationProcessor);
            return AnnotatedFieldInjectionEngine.this;
        }
    }

    //
    // Excecute
    //

    @FunctionalInterface
    public static interface FieldValueInjector {
        void inject(Field field, Object fieldValue);
    }

    public InjectionBuilder injectFields(Field[] fields) {
        return new InjectionBuilder(Arrays.asList(fields));
    }

    public class InjectionBuilder {
        private Iterable<Field> fields;

        private InjectionBuilder(Iterable<Field> fields) {
            this.fields = fields;
        }

        public void as(FieldValueInjector injector) {
            injectFields(injector, fields);
        }
    }

    private void injectFields(FieldValueInjector injector, Iterable<Field> fields) {
        for (Field field : fields) {
            injectField(injector, field);
        }
    }

    private void injectField(FieldValueInjector injector, Field field) {
        for (Annotation annotation : field.getAnnotations()) {
            Object fieldValue = getAnnotatedFieldValue(annotation, field);
            if (fieldValue != null) {
                injector.inject(field, fieldValue);
            }
        }
    }

    private Object getAnnotatedFieldValue(Annotation annotation, Field field) {
        return forAnnotation(annotation).process(annotation, field);
    }

    private <A extends Annotation> FieldAnnotationProcessor<A> forAnnotation(A annotation) {
        if (annotationProcessorMap.containsKey(annotation.annotationType())) {
            return (FieldAnnotationProcessor<A>) annotationProcessorMap.get(annotation.annotationType());
        }
        return noopProcessor();
    }

    private static final <A extends Annotation> FieldAnnotationProcessor<A> noopProcessor() {
        return (FieldAnnotationProcessor<A>) NOOP_PROCESSOR;
    }

}
