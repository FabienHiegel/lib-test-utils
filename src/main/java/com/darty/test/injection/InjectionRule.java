package com.darty.test.injection;

import java.lang.reflect.Field;

import javax.inject.Inject;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.internal.configuration.CaptorAnnotationProcessor;
import org.mockito.internal.util.reflection.FieldSetter;

import com.darty.test.injection.AnnotatedFieldInjectionEngine.FieldValueInjector;
import com.google.inject.Injector;

public class InjectionRule implements MethodRule {

    private AnnotatedFieldInjectionEngine injectionDescriptor;

    InjectionRule(Injector guiceInjector) {
        this.injectionDescriptor = new AnnotatedFieldInjectionEngine()
                .forAnnotation(Mock.class)
                .process((annotation, field) -> getResetMock(field, guiceInjector))

                .forAnnotation(Spy.class)
                .process((annotation, field) -> getResetMock(field, guiceInjector))

                .forAnnotation(Captor.class)
                .process(new CaptorAnnotationProcessor())

                .forAnnotation(Inject.class)
                .process((annotation, field) -> getFieldValue(field, guiceInjector))

                .forAnnotation(com.google.inject.Inject.class)
                .process((annotation, field) -> getFieldValue(field, guiceInjector))

                .forAnnotation(InjectMocks.class)
                .process((annotation, field) -> getFieldValue(field, guiceInjector));
    }

    private Object getFieldValue(Field field, Injector injector) {
        return injector.getInstance(field.getType());
    }

    private Object getResetMock(Field field, Injector injector) {
        Object fieldValue = getFieldValue(field, injector);
        Mockito.reset(fieldValue);
        return fieldValue;
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object testInstance) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                injectTestInstance(testInstance);
                base.evaluate();
                releaseTestInstance();
            }
        };
    }

    private void injectTestInstance(Object testInstance) {
        Field[] fields = testInstance.getClass()
                .getDeclaredFields();
        injectionDescriptor.injectFields(fields).as(setFieldToInstance(testInstance));
    }

    private FieldValueInjector setFieldToInstance(Object testInstance) {
        return (field, fieldValue) -> FieldSetter.setField(testInstance, field, fieldValue);
    }

    private void releaseTestInstance() {
        Mockito.validateMockitoUsage();
    }

}