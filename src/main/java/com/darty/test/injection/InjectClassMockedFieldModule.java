package com.darty.test.injection;

import static org.mockito.Mockito.spy;

import java.lang.reflect.Field;

import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.configuration.MockAnnotationProcessor;

import com.google.inject.AbstractModule;

class InjectClassMockedFieldModule extends AbstractModule {

    private Class<?> testClass;
    private AnnotatedFieldInjectionEngine injectionDescriptor;

    public InjectClassMockedFieldModule(Class<?> testClass) {
        this.testClass = testClass;
        this.injectionDescriptor = new AnnotatedFieldInjectionEngine()
                .forAnnotation(Mock.class)
                .process(new MockAnnotationProcessor())

                .forAnnotation(Spy.class)
                .process((annotation, field) -> spy(field.getType()));
    }

    @Override
    public void configure() {
        injectionDescriptor.injectFields(testClass.getDeclaredFields()).as(this::bindField);
    }

    private void bindField(Field field, Object fieldValue) {
        bindField(field.getType(), fieldValue);
    }

    private <F> void bindField(Class<F> declaringClass, Object fieldValue) {
        bind(declaringClass).toInstance((F) fieldValue);
    }

}