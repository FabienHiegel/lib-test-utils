package com.darty.test.server;

import javax.validation.Validation;
import javax.validation.Validator;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.dropwizard.jackson.Jackson;
import io.dropwizard.setup.Environment;

public class TestEnvironment extends Environment {

    private static final String NAME = "test-environment";

    public TestEnvironment() {
        this(NAME, Defaults.objectMapper(), Defaults.validator(), Defaults.metricRegistry(), Defaults.classLoader());
    }

    private TestEnvironment(String name, ObjectMapper objectMapper, Validator validator, MetricRegistry metricRegistry,
            ClassLoader classLoader) {
        super(name, objectMapper, validator, metricRegistry, classLoader);
    }

    private static final class Defaults {

        private static ObjectMapper objectMapper() {
            return Jackson.newObjectMapper();
        }

        private static Validator validator() {
            return Validation.buildDefaultValidatorFactory().getValidator();
        }

        private static MetricRegistry metricRegistry() {
            return new MetricRegistry();
        }

        private static ClassLoader classLoader() {
            return Thread.currentThread().getContextClassLoader();
        }
    }

    public TestEnvironment withObjectMapper(ObjectMapper objectMapper) {
        return new TestEnvironment(this.getName(), objectMapper, this.getValidator(), this.metrics(),
                this.getAdminContext().getClassLoader());
    }

    public TestEnvironment withValidator(Validator validator) {
        return new TestEnvironment(this.getName(), this.getObjectMapper(), validator, this.metrics(),
                this.getAdminContext().getClassLoader());
    }

}
